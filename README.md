### 🌿&ensp;resize tumblr iframes

#### About:
- **Acknowledgement / Shoutout:** Before I wrote this script, @⁠shythemes has a [video resizing script](https://shythemes.tumblr.com/post/134536748863/tutorial-resizing-videos), and so does @⁠nouvae's [flexibleFrames](https://github.com/robinpx/tumblr/blob/master/scripts/flexibleFrames/README.md). I've been using them for years now and although they work great most of the time, there are several iframe instances that haven't been accomodated for, especially with the rise of [NPF (Neue Post Format)](https://github.com/tumblr/docs/blob/master/npf-spec.md) posts that are gradually replacing existing post types, so I decided to write a new one.
- **Author:** HT (@ glenthemes)

---

#### Requirements:
- none (pure JavaScript, no jQuery)

---

#### How to use:
Copy the code below, and paste it in any of these places (pick one, any should work):
- after `<head>`
- above `</head>`
- after `<body>`
- above `</body>`
```html
<!--✻✻✻✻✻✻  resize iframes: @glenthemes  ✻✻✻✻✻✻-->
<script src="https://glen-iframes.gitlab.io/i/use.js"></script>
```

---

#### Credits:
- wait for element to appear in DOM tree: Yong Wang [[🔗](https://stackoverflow.com/a/61511955/8144506)]

---

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)

---

#### Found this helpful?

Consider leaving me a tip over at [ko-fi.com/glenthemes](https://ko-fi.com/glenthemes)!  
Thank you 🍓
