/*----------------------------------------------
    
    Show iframes at their intended dimensions
    gitlab.com/glen-iframes/i/-/blob/main/README.md
    - written by HT (@glenthemes)

    Last updated (v2): 2024/12/18
    
    Credits:
    - wait for element: Yong Wang
      stackoverflow.com/a/61511955/8144506
    
----------------------------------------------*/
window.glenFrames = () => {
	let glenFramesInit = () => {
		/*---- TARGET WHICH FRAMES ----*/
		let includeFrames = "iframe[width][height][data-npf*='iframe'], iframe[class*='audio_player'][width][height], .tumblr_video_container iframe[width][height], iframe[width][height][src*='youtube'], iframe[width][height][src*='vimeo'], iframe[width][height][src*='vine.co'], .tmblr-embed iframe[width][height], iframe[id^='embed-'][width][height], iframe.embed_iframe[width][height]";
		let excludeFrames = "iframe.bandcamp_audio_player, iframe[src*='bandcamp.com'], .tumblr_audio_player[class*='tumblr_audio_player_']";
		let iframesList = `*:is(${includeFrames}):not(${excludeFrames})`;

		/*---- ADD CSS ----*/
		if(!document.querySelector("style.glen-iframes-css")){
			let sty = document.createElement("style");
			sty.classList.add("glen-iframes-css");
			sty.textContent = `${includeFrames} { width:100%; max-width:100%; max-height:100%; }`;
			sty.textContent += `video, ${includeFrames} { vertical-align:middle; }`;
			sty.textContent += `.tumblr_video_container { max-width:100% }`
			sty.textContent += `.gframe-wrapper iframe { width:100%!important; height:100%!important; max-width:100%!important; max-height:100%!important }`
			sty.textContent += `.gframe-padded-height { position:relative; height:0; padding-bottom:var(--iframe-ratio-pct); z-index:0; }`
			sty.textContent += `.gframe-padded-height iframe { position:absolute; top:0;left:0; width:100%; height:100%; box-sizing:border-box; }`
			document.head.append(sty)
		}

		/*---- WAIT FOR ELEMENT(S) ----*/
		let _await=e=>new Promise(r=>{if(document.querySelector(e))return r(document.querySelector(e));let t=new MutationObserver(o=>{document.querySelector(e)&&(t.disconnect(),r(document.querySelector(e)))});t.observe(document.body,{childList:!0,subtree:!0})});

		_await(iframesList).then(() => {
			document.querySelectorAll(iframesList)?.forEach(iframe => {
				if(!iframe.matches(".processed")){
					let gframeWrapper;
					let parent = iframe.parentNode;
					if(parent && parent !== document.body){
						// if: parent has width & height (inline style)
						if(parent.matches("[style*='width:'][style*='height:']")){
							parent.style.width = "";
							parent.style.height = "";
							parent.classList.add("gframe-wrapper");
							gframeWrapper = parent;
						}

						// if: parent has no width & height inline declarations
						else {
							let wrapper = document.createElement("div");
							wrapper.classList.add("gframe-wrapper");
							iframe.before(wrapper);
							wrapper.append(iframe);
							gframeWrapper = wrapper;
						}

						// in either case, add a 'processed' class
						iframe.classList.add("processed");
					}//end: has parentNode

					let width = iframe.getAttribute("width");
					let height = iframe.getAttribute("height");
					if(!(width && height) || (width && width == 0 || height && height == 0)){
						return;
					} else {
						if(!isNaN(width) && !isNaN(height)){
							width = Number(width);
							height = Number(height);

							let aspectRatioSupp = "aspect-ratio" in document.documentElement.style;

							if(!gframeWrapper) return;

							/*---- YES: ASPECT-RATIO ----*/
							if(aspectRatioSupp){
								gframeWrapper.classList.add("gframe-aspect-ratio")
								gframeWrapper.style.aspectRatio = `${width}/${height}`
							}

							/*---- NO: ASPECT-RATIO ----*/
							else {
								let rRatio = height/width;
								gframeWrapper.style.setProperty("--iframe-ratio-pct",`${rRatio * 100}%`)
								gframeWrapper.classList.add("gframe-padded-height")
							}
						}//end: [width],[height]: nums
					}//end: [width],[height]: exist, non-zero
				}//end: iframe has NOT been processed; do things			
			})//end: iframe forEach
		})//end waitfor
	}//end glenFramesInit
	
	document.readyState == "loading" ?
	document.addEventListener("DOMContentLoaded", () => glenFramesInit()) :
	glenFramesInit();
}//end glenFrames()

glenFrames()