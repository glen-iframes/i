/*----------------------------------------------
    
    Show iframes at their intended dimensions
    - written by HT (@glenthemes)

    Last updated: 2023/08/25
    
    Credits:
    - wait for element: Yong Wang
      stackoverflow.com/a/61511955/8144506
    
----------------------------------------------*/


setTimeout(() => {
	let scriptURL = "https://static.tumblr.com/gtjt4bo/sgMrymui0/waitforelement.js";
	let sc = document.createElement("script");
	sc.src = scriptURL;
	document.head.prepend(sc);
  
    let sty = document.createElement("style");
    sty.textContent = "iframe { width:100%; max-width: 100%; }";
    sty.textContent += "video, iframe { vertical-align: middle; }";
    document.head.append(sty)
	
	sc.addEventListener("load", () => {
    let iframes = "iframe[width][height][data-npf*='iframe'], .tumblr_video_container iframe[width][height], iframe[width][height][src*='youtube'], iframe[width][height][src*='vimeo'], iframe[width][height][src*='vine.co']";
		waitForElement(iframes, { end: 3000 }).then(() => {
			document.querySelectorAll(iframes).forEach(iframe => {			    
                let w = iframe.getAttribute("width").trim();
			    let h = iframe.getAttribute("height").trim();
				
			    if(w !== "" && w !== "100%"){
					w = Number(w);
					h = Number(h);
					
					if(w !== "" && h !== ""){
						let r = h / w;
						let pw = iframe.offsetWidth;
						iframe.style.height = pw * r + "px";
						window.addEventListener("resize", () => {
							pw = iframe.offsetWidth;
							iframe.style.height = pw * r + "px"
						})
					}
				}
			})
		}).catch(err => {
			console.error(err)
		})
	})
},0)
